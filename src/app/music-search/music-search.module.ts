import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicSearchRoutingModule } from "./music-search-routing.module";
import { MusicSearchViewComponent } from "./views/music-search-view/music-search-view.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { SearchResultsComponent } from "./components/search-results/search-results.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import { API_SEARCH_URL } from "./services/tokens";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MusicSearchViewComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule, 
  
    MusicSearchRoutingModule,
    ReactiveFormsModule
  ],
  // exports: [MusicSearchViewComponent],
  providers: [
    // {
    //   provide: API_SEARCH_URL,
    //   useValue: environment.api_url
    // }
    // {
    //   provide: MusicSearchService,
    //   useFactory(url,placki, pizza){
    //     return new MusicSearchService(url)
    //   },
    //   deps:[API_SEARCH_URL, 'placki_TOKEN', 'PizaaToken']
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService,
    //   // deps:[FAKE_SEARCH_URL]
    // },{
    //   provide: MusicSearchServiceA,
    //   useClass: MusicSearchService,
    //   deps:[FAKE_SEARCH_URL_A]
    // },{
    //   provide: MusicSearchServiceB,
    //   useClass: MusicSearchService,
    //   deps:[FAKE_SEARCH_URL_B]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService
    // },
    // MusicSearchService
  ]
})
export class MusicSearchModule {}
