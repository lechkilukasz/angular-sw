import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "../../../models/Album";
import { MusicSearchService } from "../../services/music-search.service";
import { Subscription, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search-view",
  templateUrl: "./music-search-view.component.html",
  styleUrls: ["./music-search-view.component.scss"]
})
export class MusicSearchViewComponent implements OnInit {
  // results: Album[] = [];
  message: string;

  albums$ = this.service.getAlbums();
  query$ = this.service.query$;

  constructor(
    private service: MusicSearchService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    // const query = this.route.snapshot.queryParamMap.get("query");
    // if (query) {
    //   this.service.search(query);
    // }

    this.route.queryParamMap.subscribe(queryParamMap => {

      const query = queryParamMap.get("query");
      if (query) {
        this.service.search(query);
      }
      
    });
  }

  search(query) {
    this.service.search(query);

    this.router.navigate([], {
      queryParams: {
        query
      },
      relativeTo: this.route,
      replaceUrl:true
    });

  }

  ngOnInit() {}
}
