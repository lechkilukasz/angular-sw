import { InjectionToken } from "@angular/core";

export const API_SEARCH_URL = new InjectionToken("Music Search API url token");
