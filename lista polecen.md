ng g m playlists -m app --routing

ng g c playlists/views/playlists-view --export

ng g c playlists/components/items-list
ng g c playlists/components/list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form




ng g m music-search -m app --routing

ng g c music-search/views/music-search-view --export

ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card


ng g s music-search/services/music-search 

ng g m security -m app
ng g s security/auth 

